// Initializing variables
let button   = document.querySelector('#button')
let responsearea = document.querySelector('#response')
let userinput
// Event listener to set the calculate function
button.addEventListener('click', printDisplay)

// Inicializing axios
const _axios = axios.create({ baseURL: 'http://swapi.dev/api/starships/' })
const _request = requestObject => _axios.request(requestObject)

const getStarships = params => _request({ method: 'GET', params })

// Requesting data
const getAllStarships = async (index = 1) => {
    const { data: { next, results } } = await getStarships({ page: index })

    return next ? [...results, ...(await getAllStarships(index + 1))] : results
}

// Converting to HOURS
function convertConsumables(consumables) {
    // Returning 0 if the starship has no info
    if (consumables == 'unknown') return 0
    
    // Dividing consumables into two substrings (ex: 1, year) 
    const data = consumables.split(' ')
    if (data.length > 0) {
        let amount = parseInt(data[0])
        let period = data[1]
        
        // Converting to hours 
        if (period == 'year' || period == 'years') {
            return amount * 365 * 24
        } else if (period == 'month' || period == 'months') {
            return amount * 30 * 24
        } else if (period == 'week' || period == 'weeks') {
            return amount * 7 * 24
        } else if (period == 'day' || period == 'days') {
            return amount * 24
        } else if (period == 'hour' || period == 'hours') {
            return amount;
        }
    }
}

// Calculate how many times the starship would need to ressuply consumables while traveling the input distance. (Formula: input MGLT / (Starship's MGLT * Starship's Consumables converted to hours))
function countParadas(starshipMglt, starshipConsumables){
    // If there was MGLT data:
    if(starshipMglt != 'unknown'){
        return parseInt(userinput/(starshipMglt * convertConsumables(starshipConsumables)))
    } else {
        return 0
    }
}

async function printDisplay(){
    // User input
    userinput = parseInt(document.querySelector('#inputfield').value)
    // Making sure it's a number
    if(isNaN(userinput)){
        return alert('Please insert a valid distance (MGLT).')
    } else {
        // Loading starship names
        starships = await getAllStarships()
        // Adding Table Header
        responsearea.innerHTML = '<tr><th class="left">Starship</th><th class="right">Number of stops</th></tr>'
        
        // Print loop
        for (let index = 0; index < starships.length; index++) {
            responsearea.innerHTML 
            += '<tr><td class="left">' 
            + starships[index].name 
            + '</td><td class="right">'
            + countParadas(starships[index].MGLT, starships[index].consumables) 
            + '</td></tr>'
        }
    }
}



